#! python3
# getStockHistory.py - Identifies unique stocks and fetches the trade history for them


filePath = r'C:/MyPythonScripts/MyStorage/'
equityInqLink = 'https://www.nseindia.com/products/content/equities/equities/eq_security.htm'
import sys
import os
import time
import pandas as pd
import setBrowser as sb
from selenium.webdriver.support.ui import Select

#Get all file names in the directory
fileNames = os.listdir(filePath)

#Read and load the dataframe only for the first file
filedf = pd.read_csv(filePath+fileNames[0])


#Filter sold and bought stocks 
uniqueSellStocks = filedf[filedf['Buy / Sell']=='SELL']['Symbol'].unique()
uniqueBuyStocks = filedf[filedf['Buy / Sell']=='BUY']['Symbol'].unique()

#Create a browser instance and navigate to the stock inquiry page
browser = sb.setBrowser()
browser.get(equityInqLink)

#Locate the combo box. Use selenium Select class to choose "7 Days" option
try:
    select = Select(browser.find_element_by_id('dateRange'))
    select.select_by_visible_text("7 Days")
    print('Found the element')
except:
    print('Çould not find the element')

#Locate the text box to enter the stock symbol
try:
    symelem = browser.find_element_by_id('symbol')
    print('Found the element')
except:
    print('Çould not find the element')

#Locate the submit button to execute query
try:
    elem = browser.find_element_by_id('get')
    print('Found the element')
except:
    print('Çould not find the element')

#For all the stocks in the sold stock dataframe, loop over to inquire each stock
#Wait for the query to execute, then click on the download link to download file
for symbol_name in uniqueSellStocks:
    symelem.clear()
    symelem.send_keys(symbol_name)
    
    elem.click()

    time.sleep(6)

    try:
        linkelem = browser.find_element_by_class_name('download-data-link')
        print('Found the link element')
    except:
        print('Çould not find the link element')


    linkelem.click()
    time.sleep(3)
