#! python3
# getBulkTrade.py - Opens the Bulk Trade site and download previous days data

import setBrowser as sb
homePath = "C:/MyPythonScripts/"
downloadPath = "C:/MyPythonScripts/MyStorage/"
linkForBulkTrade = 'https://www.nseindia.com/products/content/equities/equities/bulk.htm'

browser = sb.setBrowser()
                    
#Navigate to the Bulk Trade Reproting web page
browser.get(linkForBulkTrade)

#Locate the submit button to execute query and click on it 
try:
    elem = browser.find_element_by_id('get')
    print('Found the element')
    elem.click()
except:
    print('Çould not find the element')

#Locate the link to the download file and click on it 
try:
    linkelem = browser.find_element_by_class_name('download-data-link')
    print('Found the link element')
    linkelem.click()
except:
    print('Çould not find the link element')

import os

#Loop through directory for all CSV files.
#Choose only the file names ending with 'k'
#Move these files to the download directory
for file in os.listdir(homePath):
    if file.endswith(".csv"):
        fname, ext = os.path.splitext(file)
          
        if fname[len(fname)-1:len(fname)] == 'k':
            print("Got it")
            os.rename(homePath+file, downloadPath+file)
        else:
            print("Did not get it") 
